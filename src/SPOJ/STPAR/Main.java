package SPOJ.STPAR;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

/**
 * Created by vreal on 22/07/15.
 */
public class Main {

    public static boolean isOrderPossible(String[] cars){
        Stack<Integer> s = new Stack<Integer>();
        int nextInOrder = 1;
//        int lastCarInStack = cars.length;
        int car;
        for(int i = 0; i < cars.length; i++){
            car = Integer.valueOf(cars[i]);
            if(car == nextInOrder){
                nextInOrder++;
            } else{
//                lastCarInStack--;
                s.push(car);
            }
            while(!s.empty() && s.peek() == nextInOrder){
                s.pop();
                nextInOrder++;
            }
        }
        return s.empty();
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] cars;
        try{
            int n = Integer.valueOf(br.readLine().trim());
            while(n != 0) {
                cars = br.readLine().trim().split(" ");
                if (isOrderPossible(cars)) {
                    System.out.println("yes");
                } else {
                    System.out.println("no");
                }
                n = Integer.valueOf(br.readLine().trim());
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
