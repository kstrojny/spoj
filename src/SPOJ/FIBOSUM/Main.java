package SPOJ.FIBOSUM;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by vreal on 04/08/15.
 */
public class Main {

    private static final int MOD = 1000000007;

    private static HashMap<Integer, Long> getSolution(int[][] input){
        HashMap<Integer, Long> solution = new HashMap<>();
        for(int i = 0; i < input.length; i++){
            fibo(input[i][0], solution);
            fibo(input[i][1], solution);
        }
        return solution;
    }

    private static long sumFibo(int n, HashMap<Integer, Long> solution){
        return fibo(n + 2, solution) - 1;
    }

    private static long fibo(int n, HashMap<Integer, Long> solution){
        if(solution.containsKey(n)){
            return solution.get(n);
        }
        if(n == 0) {
            solution.put(0, 0L);
            return 0;
        }
        if(3 > n){
            solution.put(n, 1L);
            return 1;
        }
        long answer;
        if(n % 2 == 0){
            answer = fibo(n/2, solution) * (fibo(n/2 + 1, solution) + fibo(n/2 - 1, solution));
        } else {
            long a = fibo(n / 2, solution);
            long b = fibo(n / 2 + 1, solution);
            answer = a*a + b*b;
        }
        answer = answer % MOD;
        solution.put(n, answer);
        return answer;
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] numbers;
        try{
            int n, m;
            long a, b, answer;
            int t = Integer.valueOf(br.readLine());
            HashMap<Integer, Long> solution = new HashMap<>();
            for(int i = 0; i < t; i++){
                numbers = br.readLine().split(" ");
                n = Integer.valueOf(numbers[0]) + 2 - 1;
                m = Integer.valueOf(numbers[1]) + 2;

                a = fibo(n, solution);
                b = fibo(m, solution);

                answer = (MOD + b - a) % MOD;
                System.out.println(answer);
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }

}
