package SPOJ.INVCNT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by vreal on 21/07/15.
 */
public class Main {


    public static long runCountInversions(int[] array){
        int[] helper = new int[array.length];
        int start = 0, end = array.length - 1;
        return countInversions(array, helper, start, end);
    }

    private static long countInversions(int[] array, int[] helper, int start, int end){
        long result = 0;
        if(start < end){
            int mid = start + (end - start) / 2;
            result += countInversions(array, helper, start, mid);
            result += countInversions(array, helper, mid + 1, end);
            result += mergeInversions(array, helper, start, mid, end);
        }
        return result;
    }
    //0, 3, 7, 8 || 2, 4, 7
    private static long mergeInversions(int[] array, int[] helper, int start, int mid, int end){
        long result = 0;
        for(int n = start; n <= end; n++){
            helper[n] = array[n];
        }
        int i = mid, j = end, p = end;
        while(i >= start && j > mid){
            if(helper[i] > helper[j]){
                array[p] = helper[i];
                result += (j - mid);
                p--;
                i--;
            }else {
                array[p] = helper[j];
                p--;
                j--;
            }
        }
        while(i >= start){
            array[p] = helper[i];
            //dont modify result
            p--;
            i--;
        }
        while(j > mid){
            array[p] = helper[j];
            //dont modify result
            p--;
            j--;
        }
        return result;
    }

    public static void runMergeSort(int[] array){
        int[] helper = new int[array.length];
        mergeSort(array, helper, 0, array.length - 1);
    }

    private static void mergeSort(int[] array, int[] helper, int start, int end){
        if(start < end) {
            int mid = start + (end - start) / 2;
            mergeSort(array, helper, start, mid);
            mergeSort(array, helper, mid + 1, end);
            merge(array, helper, start, mid, end);
        }
    }

    private static void merge(int[] array, int[] helper, int start, int mid, int end){
        for(int n = start; n <= end; n++){
            helper[n] = array[n];
        }
        int i = mid, j = end;  //pointers to end of helper
        int p = end;  //pointer to end of unsorted array
        while(i >= start && j > mid){
            if(helper[i] > helper[j]){
                array[p] = helper[i];
                p--;
                i--;
            } else {
                array[p]  = helper[j];
                p--;
                j--;
            }
        }
        while(i >= start){
            array[p] = helper[i];
            p--;
            i--;
        }
        while(j > mid){
            array[p] = helper[j];
            p--;
            j--;
        }
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            String line = br.readLine();
            int t = Integer.valueOf(line);
            for(int ii = 0; ii < t; ii++){
                do {
                    line = br.readLine();
                } while(line.isEmpty());
                int n = Integer.valueOf(line);
                int[] array = new int[n];
                for(int k = 0; k < n ; k++){
                    array[k] = Integer.valueOf(br.readLine());
                }
                System.out.println(runCountInversions(array));
            }

        }catch(IOException e){
            e.printStackTrace();
        }

//        Random r = new Random();
//        int n = 200000;
//        int[] array = new int[n];
//        for(int i = 0; i < n; i++){
//            array[i] = r.nextInt(10000000);
//        }
//        long start = System.currentTimeMillis();
//        System.out.println(runCountInversions(array));
//        long end = System.currentTimeMillis();
//        System.out.printf("%f\n", (end - start) / 1000000.);

    }
}
