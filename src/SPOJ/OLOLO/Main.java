package SPOJ.OLOLO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by vreal on 24/08/15.
 */
public class Main {
    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            int p, n = Integer.valueOf(br.readLine());
            int answer = 0;
            for(int i = 0; i < n; i++){
                p = Integer.valueOf(br.readLine());
                answer = answer ^ p;
            }
            System.out.println(answer);
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
