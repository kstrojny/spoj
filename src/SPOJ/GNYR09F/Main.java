package SPOJ.GNYR09F;

import java.util.Scanner;

/**
 * Created by vreal on 14/07/15.
 */
public class Main {


    //11100, 01110, 00111, 10111, 11101, 11011
    //1100 0110    f(4, 1, true) = 2
    //0011 1101 1011 f(4, 1, false) =3
    //1110    f(4, 2, true) = 1
    //0111    f(4, 2, false) = 1

    //AdjBC(x)
    //f(x, y) = f(x, y, true) + f(x, y, false)
    //f(x, y, true) = f(x - 1, y, true) + f(x - 1, y, false)
    //f(x, y, false) = f(x - 1, y - 1, false) + f(x - 1, y, true)
    //f(n, n-1, true) = 0
    //f(n, n-2, true) = 1
    //f(n, n-1, false) = 1
    /*


    f(3, 1) == 2
     110 011
     f(3, 1, true) 110
     f(3, 1, false) 011 = f(2, 0, false) + f(2,1, true)

    */

    public static int numberOfStrings(int n, int k){
        int[][][] array = new int[n + 1][k + 1][2];
        int a = numberOfStrings(n, k, true, array);
        int b = numberOfStrings(n, k, false, array);
        return a + b;
    }


    //11100, 01110
    public static int numberOfStrings(int n, int k, boolean endsWithZero, int[][][] array){
        if(k == n - 1){
            if(endsWithZero){
                if(n == 1){
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 1;
            }
        }
        if (k < 0) {
            return 0;
        }
        if(endsWithZero && array[n][k][1] != 0) {
            return array[n][k][1];
        } else if(!endsWithZero && array[n][k][0] != 0) {
            return array[n][k][0];
        }
        int tmp;
        if(endsWithZero){
            tmp = numberOfStrings(n - 1, k, false, array) + numberOfStrings(n - 1, k, true, array);
            array[n][k][1] = tmp;
        } else {  //  end with  one
            tmp = numberOfStrings(n - 1, k - 1, false, array) + numberOfStrings(n - 1, k, true, array);
            array[n][k][0] = tmp;
        }
        return tmp;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int p = sc.nextInt();
        int[][] input = new int[p][2];
        for(int n = 0; n < p; n++){
            sc.nextInt();
            input[n][0] = sc.nextInt();
            input[n][1] = sc.nextInt();
        }
        for(int n = 0; n < p; n++) {
            System.out.printf("%d %d\n", n + 1, numberOfStrings(input[n][0], input[n][1]));
        }
    }
}
// 0000, 1000, 0100, 0010, 0001, 1010, 1001, 0101

// 00, 10, 01