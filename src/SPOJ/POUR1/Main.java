package SPOJ.POUR1;

/**
 * Created by vreal on 09/06/15.
 * Given two vessels, one of which can accommodate a litres of water and the other - b litres of water, determine the
 * number of steps required to obtain exactly c litres of water in one of the vessels.

 At the beginning both vessels are empty. The following operations are counted as 'steps':

 emptying a vessel,
 filling a vessel,
 pouring water from one vessel to the other, without spilling, until one of the vessels is either full or empty.
 Input

 An integer t, 1<=t<=100, denoting the number of testcases, followed by t sets of input data, each consisting of three
 positive integers a, b, c, not larger than 40000, given in separate lines.
 */
public class Main {
//    c = a - k*b
//    a - c = k* b
    public static int nwd(int x, int y) {
        while (x != y) {
            if (x > y)
                x -= y;
            else
                y -= x;
        }
        return x;
    }

    public static int nww(int x, int y){
        return (x * y) / nwd(x, y);
    }

    private static int calcMinSteps(int a, int b, int c){
        int x = calcSteps(a, b, c);//dolewanie
        int y = calcSteps(b, a, c);//odlewanie
        if(x == -1)
            return y;
        if(y == -1)
            return x;
        return Math.min(x, y);
    }

    private static int calcSteps(int a, int b, int c){
        if(Math.max(a, b) < c)
            return -1;
        int nww = nww(a, b);
        int k_limit = nww / b;
        int l_limit = nww / a;
        int k = 0, l = 0;
        while(k < k_limit && l < l_limit){
            if(b * k == a * l + c){
                return k + l;   // 4 1 | 2 3
            } else if(b * k > a * l + c){
                l++;
            } else {
                k++;
            }
        }
        return -1;
    }

    private static int bruteSolution(){

        return 0;
    }

    private static void test(boolean isCorrect){
        if(isCorrect){
            System.out.println("ok");
        } else {
            System.err.println("FAIL!!!!");
        }
    }

    private static void testCalcSteps(){
//        test(calcMinSteps(5, 2, 3) ==  2);
//        test(calcMinSteps(3, 2, 4) ==  -1);
//        test(calcMinSteps(7, 3, 4) ==  2);
//        test(calcMinSteps(7, 3, 3) ==  1);
        test(calcMinSteps(7, 3, 5) ==  8);
        System.out.println("Sukces");
    }

    public static void main(String[] args){
        testCalcSteps();

    }
}
