package SPOJ.FCTRL2;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by vreal on 09/06/15.
 * An integer t, 1<=t<=100, denoting the number of testcases, followed by t lines, each containing a single integer n, 1<=n<=100.

 Output

 For each integer n given at input, display a line with the value of n!
 */
public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        List<Integer> input = new ArrayList<Integer>();
        for(int i = 0; i < t; i++){
           input.add(scanner.nextInt());
        }
        int max = Collections.max(input);
        BigInteger[] results = new BigInteger[max + 1];
        results[1] = BigInteger.ONE;
        for(int n = 2; n < max + 1; n++){
            results[n] = results[n-1].multiply(BigInteger.valueOf((long) n));
        }
        for(Integer inputNumber : input){
            System.out.println(results[inputNumber]);
        }
    }
}
