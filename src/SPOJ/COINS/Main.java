package SPOJ.COINS;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by vreal on 24/06/15.
 */
public class Main {

    private static Map<Integer, Long> countedMax = new HashMap<Integer, Long>();

    private static long getMax(int n){
        if(countedMax.containsKey(n))
            return countedMax.get(n);
        if (n/2 + n/3 + n/4 > n){
            long max = getMax(n/2) + getMax(n/3) + getMax(n/4);
            countedMax.put(n, max);
            return max;
        } else {
            countedMax.put(n, (long)n);
            return n;
        }
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()) {
            System.out.println(getMax(sc.nextInt()));
        }
//        System.out.println(getMax(0));
    }
}
