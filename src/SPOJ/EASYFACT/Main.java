package SPOJ.EASYFACT;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by vreal on 23/06/15.
 */
public class Main {

    private static int calc_algo(int n, int m){
        int odd_numbers = (n-1)/2;
        int power = 2;
        int prev = 1;
        while(odd_numbers > 1) {
            if (odd_numbers % 2 == 0) {
                power = power * power;
                odd_numbers = odd_numbers / 2;
            } else {
                prev = prev * power;
                odd_numbers = odd_numbers - 1;
            }
            if (prev > m) {
                prev = prev % m;
            }
            if (power > m) {
                power = power % m;
            }
        }
        return (prev * power - 1) % m;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> input = new ArrayList<Integer>();
        int t = scanner.nextInt();
        for (int n = 0; n < t; n++) {
            input.add(scanner.nextInt());
            input.add(scanner.nextInt());
        }
        for(int index = 0; index < input.size(); index += 2) {
            System.out.println(Main.calc_algo(input.get(index), input.get(index + 1)));
        }
//        assert Wrong.calc_algo(3, 7) == 1;
//        assert Wrong.calc_algo(5, 7) == 3;
//        assert Wrong.calc_algo(7, 17) == 7;  WRONG, its 6, number have to be positive
//        System.out.println(Wrong.calc_algo(3, 7));
    }

}
