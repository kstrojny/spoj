package SPOJ.SAMER08F;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by vreal on 21/07/15.
 */
public class Main {

    public static int feymannRiddle(int n){
        int answer = 0;
        for(int i = n; i > 0; i--){
            answer += i * i;
        }
        return answer;
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            int n = Integer.valueOf(br.readLine().trim());
            while(n > 0) {
                System.out.println(feymannRiddle(n));
                n = Integer.valueOf(br.readLine().trim());
            }
        } catch(IOException e){
            e.printStackTrace();
        }
//        System.out.println(feymannrRiddle(8));
//        System.out.println(feymannrRiddle(1));
    }
}
