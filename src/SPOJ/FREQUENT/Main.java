package SPOJ.FREQUENT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by vreal on 24/08/15.
 */
public class Main {

    static class Node{
        int longest;
        int rightSize;
        int leftSize;
    }

    private static int leftSon(int index){
        return 2 * index + 1;
    }

    private static int rightSon(int index){
        return 2 * index + 2;
    }

    private static Node[] buildTree(int[] sequence){
        int h = (int)Math.ceil(Math.log(sequence.length) / Math.log(2)) + 1;
        int size = (int)Math.pow(2, h) - 1;
        Node[] tree = new Node[size];
        updateNode(tree, sequence, 0, 0, sequence.length - 1);
        return tree;
    }

    private static Node updateNode(Node[] tree, int[] sequence, int nodeIndex, int leftRage, int rightRange){
        if(nodeIndex < tree.length) {
            if(leftRage == rightRange){
                Node result = new Node();
                result.longest = result.leftSize = result.rightSize = 1;
                tree[nodeIndex] = result;
                return result;
            } else {
                int mid = (leftRage + rightRange) / 2;
                Node left = updateNode(tree, sequence, leftSon(nodeIndex), leftRage, mid);
                Node right = updateNode(tree, sequence, rightSon(nodeIndex), mid + 1, rightRange);
                Node result = merge(sequence, left, leftRage, mid, right, mid + 1, rightRange);
                tree[nodeIndex] = result;
                return result;
            }
        }
        return null;
    }

    private static Node merge(int[] sequence, Node n1, int n1LeftRange, int n1RightRange,
                              Node n2, int n2LeftRange, int n2RightRange){
        Node result = new Node();
        if(n1 == null || n2 == null){
            Node n = (n1 != null) ? n1 : n2;
            result.longest = n.longest;
            result.rightSize = n.rightSize;
            result.leftSize = n.leftSize;
        } else {
            result.longest = Math.max(n1.longest, n2.longest);
            if (sequence[n1RightRange] == sequence[n2LeftRange]) {
                result.longest = Math.max(result.longest, n1.rightSize + n2.leftSize);
            }
            if (sequence[n1LeftRange] == sequence[n2LeftRange]) {
                result.leftSize = (n1RightRange - n1LeftRange + 1) + n2.leftSize;
            } else {
                result.leftSize = n1.leftSize;
            }
            if (sequence[n2RightRange] == sequence[n1RightRange]) {
                result.rightSize = (n2RightRange - n2LeftRange + 1) + n1.rightSize;
            } else {
                result.rightSize = n2.rightSize;
            }
        }
        return result;
    }

    public static int answerQuery(Node[] tree, int[] sequence, int i, int j){
        Node result = runAnswerQuery(tree, sequence, 0, 0, sequence.length - 1, i, j);
        return result.longest;
    }

    private static Node runAnswerQuery(Node[] tree, int[] sequence, int currentNode, int left, int right, int i, int j){
        if(i <= left && right <= j){ //totally in range
            return tree[currentNode];
        } else if(right < i || j < left){ //totally outside range
            return null;
        } else { //partially in range
            int mid = (left + right) / 2;
            Node leftAnswer = runAnswerQuery(tree, sequence, leftSon(currentNode), left, mid, i, j);
            Node rightAnswer = runAnswerQuery(tree, sequence, rightSon(currentNode), mid + 1, right, i, j);
            return merge(sequence, leftAnswer, left, mid, rightAnswer, mid + 1, right);
        }
    }

    private static void stressTest(){
        Random r = new Random();
        int n = (int) 1e5;
        int q = (int) 1e5;
        int[] sequence = new int[n];
        for(int i = 0; i < n; i++){
            sequence[i] = r.nextInt(10000) - 5000;
        }
        Arrays.sort(sequence);
        long startTime = System.nanoTime();
        Node[] tree = buildTree(sequence);
        long endTree = System.nanoTime();
        for(int i = 0; i < q; i++){
            int start = r.nextInt(n);
            int end = start + r.nextInt(n - start);
            answerQuery(tree, sequence, start, end);
        }
        long endQuery = System.nanoTime();
        System.out.printf("Building tree took %f ms\n", (endTree - startTime) / 1e6);
        System.out.printf("Answering queries took %f ms\n", (endQuery - endTree) / 1e6);
    }

    public static void main(String[] args){
//        stressTest();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;
        String[] numbers;
        try{
            while(true) {
                input = br.readLine();
                if (input.length() > 2) {
                    numbers = input.split(" ");
                    int n = Integer.valueOf(numbers[0]);
                    int q = Integer.valueOf(numbers[1]);
                    int[] sequence = new int[n];
                    numbers = br.readLine().split(" ");
                    for (int ii = 0; ii < n; ii++) {
                        sequence[ii] = Integer.valueOf(numbers[ii]);
                    }
                    Node[] tree = buildTree(sequence);
                    for (int ii = 0; ii < q; ii++) {
                        numbers = br.readLine().split(" ");
                        int i = Integer.valueOf(numbers[0]) - 1;
                        int j = Integer.valueOf(numbers[1]) - 1;
                        System.out.println(answerQuery(tree, sequence, i, j));
                    }
                } else {
                    break;
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
