package SPOJ.HORRIBLE;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vreal on 15/07/15.
 */
public class Main {

    static class Node{
        public long value;
        public long sonsLegacy;
        public boolean needUpdate;
        public boolean needUpdateSons;

        public Node(long value, boolean needUpdate, boolean needUpdateSons){
            this.value = value;
            this.needUpdate = needUpdate;
            this.needUpdateSons = needUpdateSons;
            this.sonsLegacy = 0;
        }
    }

    private static int father(int index) {
        return (index - 1) / 2;
    }

    private static int leftSon(int index){
        return 2 * index + 1;
    }

    private static int rightSon(int index){
        return 2 * index + 2;
    }

    public static void runAddQuery(int p, int q, int v, Node[] tree, int n){
        addQuery(p - 1, q - 1, v, 0, n - 1, 0, tree);
    }

    private static void addQuery(int p, int q, int v, int leftRange, int rightRange, int actualNodeIndex,  Node[] tree){
        int mid = leftRange + (rightRange - leftRange) / 2;
        if(p <= leftRange && rightRange <= q){
            tree[actualNodeIndex].value += v * (long)(rightRange - leftRange + 1);
            if(leftRange < rightRange) {  //check if we have to update children
                tree[actualNodeIndex].needUpdateSons = true;
                tree[actualNodeIndex].sonsLegacy += v;
//                addQuery(p, q, v, leftRange, mid, leftSon(actualNodeIndex), tree);
//                addQuery(p, q, v, mid +1, rightRange, rightSon(actualNodeIndex), tree);
            }
        } else if(rightRange < p || q < leftRange) {  //outside range
            //do nothing
        } else {
            //partly in range
            tree[actualNodeIndex].needUpdate = true;
            addQuery(p, q, v, leftRange, mid, leftSon(actualNodeIndex), tree);
            addQuery(p, q, v, mid + 1, rightRange, rightSon(actualNodeIndex), tree);
        }
    }

    public static long runGetSum(int p, int q, Node[] tree, int n){
        return getSum(p - 1, q - 1, 0, n - 1, 0, tree);
    }

    private static long getSum(int p, int q, int leftRange, int rightRange, int actualNodeIndex, Node[] tree){
        if(rightRange < p || q < leftRange){  //outside range
            return 0L;
        }
        int mid = leftRange + (rightRange - leftRange) / 2;
        if(tree[actualNodeIndex].needUpdateSons && leftRange < rightRange){

            int l = leftSon(actualNodeIndex);
            int r = rightSon(actualNodeIndex);
            tree[l].needUpdateSons = true;
            tree[l].sonsLegacy += tree[actualNodeIndex].sonsLegacy;
            tree[l].value += tree[actualNodeIndex].sonsLegacy * (mid - leftRange + 1);

            tree[r].needUpdateSons = true;
            tree[r].sonsLegacy += tree[actualNodeIndex].sonsLegacy;
            tree[r].value += tree[actualNodeIndex].sonsLegacy * (rightRange - (mid + 1) + 1);  //know we can optimise it

            tree[actualNodeIndex].needUpdateSons = false;
            tree[actualNodeIndex].sonsLegacy = 0;

        }
        if(p <= leftRange && rightRange <= q) {
            if(tree[actualNodeIndex].needUpdate){
                long sum = getSum(p, q, leftRange, mid, leftSon(actualNodeIndex), tree) +
                        getSum(p, q, mid + 1, rightRange, rightSon(actualNodeIndex), tree);
                tree[actualNodeIndex].value = sum;
                tree[actualNodeIndex].needUpdate = false;
                return sum;
            } else {
                return tree[actualNodeIndex].value;
            }
        }
        //  partly inside the range,

        return getSum(p, q, leftRange, mid, leftSon(actualNodeIndex), tree) +
                getSum(p, q, mid + 1, rightRange, rightSon(actualNodeIndex), tree);
    }


    public static void main(String[] args) {
//        int n = 100000;
//        int c = 100000;
//        int height = (int)Math.ceil(Math.log(n) / Math.log(2));
//        int size = 2 * (int)Math.pow(2, height) - 1;
//        Node[] tree = new Node[size];
//        for(int i = 0; i < tree.length; i++){
//            tree[i] = new Node(0, false, false);
//        }
//
//        Random r = new Random();
//        boolean[] boolArray = new boolean [c];
//        int[] pArray = new int [c];
//        int[] qArray = new int [c];
//        int[] vArray = new int [c];
//        for(int ii = 0; ii < c; ii++){
//            boolArray[ii] = true;
//            pArray[ii] = 1;
//            qArray[ii] = n;
//            vArray[ii] = (int)1e7;
//        }
//        System.out.println("Start");
//        long start = System.nanoTime(), startAdd, sumAdd = 0L, startSum, sumSum = 0L;
//        for(int ii = 0; ii < c; ii++){
//            if(boolArray[ii]){
//                startAdd = System.nanoTime();
//                runAddQuery(pArray[ii], qArray[ii], vArray[ii], tree, n);
//                sumAdd += (System.nanoTime() - startAdd);
//
//            } else {
//                startSum = System.nanoTime();
//                runGetSum(pArray[c], qArray[c], tree, n);
//                sumSum += (System.nanoTime() - startSum);
//            }
//        }
//        startSum = System.nanoTime();
//        System.out.println(runGetSum(1, n, tree, n));
//        sumSum += (System.nanoTime() - startSum);
//
//
//        long end = System.nanoTime();
//        System.out.println((end - start) / 1000000.);
//        System.out.println(sumAdd / 1000000.);
//        System.out.println(sumSum / 1000000.);
//        runAddQuery(1, 1, 10, tree, n);
//        runAddQuery(1, 1, 10, tree, n);
//        runAddQuery(4, 5, 20, tree, n);
//        System.out.println(runGetSum(8, 8, tree, n));
//        runAddQuery(5, 7, 14, tree, n);
//        System.out.println(runGetSum(1, 2, tree, n));
//        System.out.println(runGetSum(2, 9, tree, n));
//        System.out.println(runGetSum(3, 9, tree, n));
//        System.out.println(runGetSum(4, 8, tree, n));
        try {
            List<Integer> output = new ArrayList<Integer>();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in), 10000);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out), 1000);
            int p, q, v;
            String[] command;
            int t = Integer.valueOf(reader.readLine());
            for (int i = 0; i < t; i++) {
                command = reader.readLine().split(" ");
                int n = Integer.valueOf(command[0]);
                int c = Integer.valueOf(command[1]);
                int height = (int) Math.ceil(Math.log(n) / Math.log(2));
                int size = 2 * (int) Math.pow(2, height) - 1;
                Node[] tree = new Node[size];
                for (int ii = 0; ii < tree.length; ii++) {
                    tree[ii] = new Node(0, false, false);
                }
                for (int ii = 0; ii < c; ii++) {
                    command = reader.readLine().split(" ");
                    if ("0".equals(command[0])) {
                        p = Integer.valueOf(command[1]);
                        q = Integer.valueOf(command[2]);
                        v = Integer.valueOf(command[3]);
                        runAddQuery(p, q, v, tree, n);
                    } else if ("1".equals(command[0])) {
                        p = Integer.valueOf(command[1]);
                        q = Integer.valueOf(command[2]);
                        long answer = runGetSum(p, q, tree, n);
                        System.out.println(answer);
//                        writer.write(Integer.toString(answer) + "\n");
                    }
                }
            }
            writer.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
