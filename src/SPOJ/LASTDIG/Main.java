package SPOJ.LASTDIG;
import java.util.Scanner;

public class Main {
    static int[] cN = {1,1,4,4,2,1,1,4,4,2};
    static int aToB(int a, int b){
        if(b==0)
            return 1;
        a%=10;
        int tmp = 1, i = b % cN[a];
        if(i == 0)
            i = cN[a];
        for(int n = 0; n < i; n++)
            tmp *= a;
        return tmp%10;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for(int n = 0; n < t; n++){
            System.out.println(aToB(sc.nextInt(), sc.nextInt()));
        }
    }
}
