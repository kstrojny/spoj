package SPOJ.ABCDEF;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Random;
import java.util.TreeMap;

/**
 * Created by vreal on 25/08/15.
 */
public class Main {

    private static TreeMap<Integer, Integer>[] getTreeMaps(int[] set){
        int n = set.length;
        TreeMap<Integer, Integer> abc = new TreeMap<>();
        TreeMap<Integer, Integer> def = new TreeMap<>();
        Integer leftSideAbc, leftSideDef;
        Integer count;
        for(int a = 0; a < n; a++){
            for(int b = 0; b < n; b++){
                for(int c = 0; c < n; c++){
                    leftSideAbc = set[a] * set[b] + set[c];
                    count = abc.get(leftSideAbc);
                    if(count == null){
                        abc.put(leftSideAbc, 1);
                    } else {
                        abc.put(leftSideAbc, count + 1);
                    }
                    if(set[a] != 0) {
                        leftSideDef = set[a] * (set[b] + set[c]);
                        count = def.get(leftSideDef);
                        if (count == null) {
                            def.put(leftSideDef, 1);
                        } else {
                            def.put(leftSideDef, count + 1);
                        }
                    }
                }
            }
        }
        TreeMap[] result = new TreeMap[2];
        result[0] = abc;
        result[1] = def;
        return result;
    }

    private static TreeMap<Integer, Integer> getDef(int[] set){
        int n = set.length;
        TreeMap<Integer, Integer> def = new TreeMap<>();
        Integer leftSideDef;
        Integer count;
        for(int d = 0; d < n; d++){
            for(int e = 0; e < n; e++){
                for(int f = 0; f < n; f++){
                    if(set[d] != 0) {
                        leftSideDef = set[d] * (set[e] + set[f]);
                        count = def.get(leftSideDef);
                        if (count == null) {
                            def.put(leftSideDef, 1);
                        } else {
                            def.put(leftSideDef, count + 1);
                        }
                    }
                }
            }
        }
        return def;
    }

    private static int countPossibilities(int[] set){
        int result = 0;
        long startTreeSet = System.nanoTime();
        TreeMap[] array = getTreeMaps(set);
        TreeMap<Integer, Integer> abcTreeMap = array[0];
        long firstEnd = System.nanoTime();
        TreeMap<Integer, Integer> defTreeMap = array[1];
        long secondEnd = System.nanoTime();
        System.out.printf("First = %f, Second = %f\n",(firstEnd - startTreeSet) / 1e6, (secondEnd - firstEnd) / 1e6 );
        NavigableSet<Integer> navigableSetAbc = abcTreeMap.navigableKeySet();
        NavigableSet<Integer> navigableSetDef = defTreeMap.navigableKeySet();
        Iterator<Integer> abcIterator = navigableSetAbc.iterator();
        Iterator<Integer> defIterator = navigableSetDef.iterator();
        Integer a = abcIterator.next();
        Integer d = defIterator.next();
        while(abcIterator.hasNext() && defIterator.hasNext()){
            if(a.equals(d)){
                result += abcTreeMap.get(a) * defTreeMap.get(d);
                a = abcIterator.next();
                d = defIterator.next();
            } else if(a > d){
                d = defIterator.next();
            } else {
                a = abcIterator.next();
            }
        }
        if(a.equals(d)) {
            result += abcTreeMap.get(a) * defTreeMap.get(d);
        }
        return result;
    }

    private static void stressTest(){
        int n = 100;
        Random r = new Random();
        int[] set = new int[n];
        for(int i = 0; i < n; i++){
            set[i] = r.nextInt(60000) - 30000;
        }
        long startTime = System.nanoTime();
        countPossibilities(set);
        long endTime = System.nanoTime();
        System.out.printf("Test took %f ms\n", (endTime - startTime) / 1e6);
    }

    public static void main(String[] args){
        stressTest();
//        BufferedReader  br = new BufferedReader(new InputStreamReader(System.in));
//        try{
//            int n = Integer.valueOf(br.readLine());
//            int[] set = new int[n];
//            for(int i = 0; i < n; i++){
//                set[i] = Integer.valueOf(br.readLine());
//            }
//            System.out.println(countPossibilities(set));
//        } catch(IOException e){
//            e.printStackTrace();
//        }
    }
}
