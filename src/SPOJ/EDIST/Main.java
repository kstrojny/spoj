package SPOJ.EDIST;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by vreal on 21/07/15.
 */
public class Main {

    //memory[i][j] distance bettwen first i letters of 'a' word and first j letters from 'b' word
    public static int editDistance(String a, String b){
        int ifDel, ifChange, ifAdd;
        int[][] memory = new int[a.length() + 1][b.length() + 1];
        for(int i = 0; i < a.length() + 1; i++){
            for(int j = 0; j < b.length() + 1; j++){
                if(i == 0){
                    memory[i][j] = j;
                } else if(j == 0){
                    memory[i][j] = i;
                } else {
                    if(a.charAt(i - 1) == b.charAt(j - 1)){
                        memory[i][j] = memory[i - 1][j - 1];
                    } else {
                        ifDel = memory[i][j - 1] + 1;
                        ifAdd = memory[i - 1][j] + 1;
                        ifChange = memory[i - 1][j - 1] + 1;
                        memory[i][j] = Math.min(ifAdd, Math.min(ifDel, ifChange));
                    }
                }
            }
        }
        return memory[a.length()][b.length()];
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            int t = Integer.valueOf(br.readLine());
            for(int n = 0; n < t; n++){
                String a = br.readLine();
                String b = br.readLine();
                System.out.println(editDistance(a, b));
            }
        } catch (IOException e){
            e.printStackTrace();
        }
//        System.out.println(editDistance("FOOD", "MONEY"));
    }
}
