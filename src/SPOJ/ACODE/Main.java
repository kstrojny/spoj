package SPOJ.ACODE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by vreal on 21/07/15.
 */
public class Main {

    // 'z' = 26
    public static int getNumberPossibleDecoding(String a){
        int[] memory = new int[a.length()];
        memory[0] = 1;
        if(a.length() > 1){
            if(a.charAt(1) == '0' && a.charAt(0) == '0')
                return 0;
            if(a.charAt(1) == '0' && a.charAt(0) > '2'){
                return 0;
            }
            if(a.charAt(1) != '0' && (a.charAt(0) < '2' || (a.charAt(0) == '2' && a.charAt(1) <= '6'))){
                memory[1] = 2;
            } else {
                memory[1] = 1;
            }
        }
        for(int n = 2; n < a.length(); n++){
            if(a.charAt(n) == '0' && a.charAt(n - 1) == '0')
                return 0;
            if(a.charAt(n) == '0' && a.charAt(n - 1) > '2'){
                return 0;
            }
            if(a.charAt(n - 1) != '0' && a.charAt(n) != '0' && (a.charAt(n - 1) < '2' || (a.charAt(n - 1) == '2') && a.charAt(n) <= '6')){
                memory[n] = memory[n - 1] + memory[n - 2];
            } else if(a.charAt(n) == '0'){
                memory[n] = memory[n - 2];
            } else {
                memory[n] = memory[n- 1];
            }
        }
        return memory[a.length() - 1];
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            String line = br.readLine().trim();
            while(!line.equals("0")) {
                System.out.println(getNumberPossibleDecoding(line));
                line = br.readLine().trim();
            }
        } catch(IOException e){
            e.printStackTrace();
        }

//        StringBuilder b = new StringBuilder();
//        for(int i = 0; i < 5000; i++){
//            b.append('1');
//        }
//        String line = b.toString();
//        System.out.println(line);
//        System.out.println(getNumberPossibleDecoding(line));

//        System.out.println(getNumberPossibleDecoding("25114"));
//        System.out.println(getNumberPossibleDecoding("20"));        //should be 1
//        System.out.println(getNumberPossibleDecoding("21"));        //should be 2
//        System.out.println(getNumberPossibleDecoding("101"));        //should be 1
//        System.out.println(getNumberPossibleDecoding("1111111111"));
//        System.out.println(getNumberPossibleDecoding("3333333333"));
    }

}
