package SPOJ.FAVDICE;

import java.util.Scanner;

/**
 * Created by vreal on 14/07/15.
 */
public class Main {

    public static double expectedVal(int n, double[] harmonical){
        return n * harmonical[n];
    }

    public static double[] createHarmonical(int l){
        double[] harmonical = new double[l];
        harmonical[1] = 1;
        for(int n = 2; n < l; n++){
            harmonical[n] = harmonical[n - 1] + (1. / n);
        }
        return harmonical;
    }

    public static void main(String[] args){
        int n, max = 0, tmp;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        int[] input = new int[n];
        for(int i = 0; i < n; i++){
            tmp = sc.nextInt();
            max = Math.max(max, tmp);
            input[i] = tmp;
        }
        double[] harmonical = createHarmonical(max + 1);
        for(int i = 0; i < n; i++){
            System.out.printf("%.2f\n", expectedVal(input[i], harmonical));
        }
//        System.out.printf("%.2f\n", expectedVal(1, harmonical));
//        System.out.printf("%.2f\n", expectedVal(12, harmonical));
//        System.out.println(expectedVal(50, harmonical));
    }
}
