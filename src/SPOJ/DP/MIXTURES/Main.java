package SPOJ.DP.MIXTURES;

import java.util.Scanner;

/**
 * Created by vreal on 27/07/15.
 */
public class Main {

    private static class Result{
        int smoke;
        int mixture;

        private Result(int smoke, int mixture){
            this.smoke = smoke;
            this.mixture = mixture;
        }
    }

    public static Result minimumSmoke(int[] mixtures, Result[][] minSmoke, int start, int end){
        if(start == end){
             return new Result(0, mixtures[start]);
        }
        if(start < end) {
            if(minSmoke[start][end] != null){
                return minSmoke[start][end];
            }
            int smoke, mixture = 0, min = Integer.MAX_VALUE;
            Result result1, result2;
            for (int n = start; n < end; n++) {
                result1 = minimumSmoke(mixtures, minSmoke, start, n);
                result2 = minimumSmoke(mixtures, minSmoke, n + 1, end);
                smoke = result1.smoke + result2.smoke + result1.mixture * result2.mixture;
                if(min > smoke){
                    min = smoke;
                    mixture = (result1.mixture + result2.mixture) % 100;
                }
            }
            minSmoke[start][end] = new Result(min, mixture);
            return minSmoke[start][end];
        } else {
            return null;
        }
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            int t = scanner.nextInt();
            int[] mixtures = new int[t];
            Result[][] memory = new Result[t][t];
            for (int n = 0; n < t; n++) {
                mixtures[n] = scanner.nextInt();
            }
            System.out.println(minimumSmoke(mixtures, memory, 0, t - 1).smoke);
        }

//        int[] mixtures = {1,2,3,4};
//        Result[][] memory = new Result[mixtures.length][mixtures.length];
//        Result result = minimumSmoke(mixtures, memory, 0, mixtures.length - 1);
//        System.out.println(result.smoke);
    }
}
