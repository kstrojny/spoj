package SPOJ.DP.FARIDA;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

;

/**
 * Created by vreal on 22/07/15.
 */
public class Main {


    public static long maxGold(int[] monsters){
        if(monsters.length == 0){
            return 0;
        }
        long[] memory = new long[monsters.length];
        memory[0] = monsters[0];
        if(monsters.length > 1){
            memory[1] = Math.max(monsters[0], monsters[1]);
        }
        for(int i = 2; i < monsters.length; i++){
            memory[i] = Math.max(memory[i - 1], memory[i - 2] + monsters[i]);
        }
        return memory[monsters.length - 1];
    }

    public static void main(String[] args){
        String[] numbers;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        List<Long> result;
        try{
            int t = Integer.valueOf(br.readLine().trim());
            result = new ArrayList<Long>(t);
            for(int i = 0; i < t; i++){
                int n = Integer.valueOf(br.readLine().trim());
                int[] monsters = new int[n];
                numbers = br.readLine().trim().split(" ");
                for(int k = 0; k < n; k++){
                    monsters[k] = Integer.valueOf(numbers[k]);
                }
                result.add(maxGold(monsters));
            }
            for(int i = 0; i < t; i++){
                System.out.printf("Case %d: %d\n", i + 1, result.get(i));
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
