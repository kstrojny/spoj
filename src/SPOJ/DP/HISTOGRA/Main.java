package SPOJ.DP.HISTOGRA;

/**
 * Created by vreal on 08/08/15.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static long slowMaxHistogram(int[] array){
        long max = Long.MIN_VALUE;
        for(int start = 0; start < array.length; start++){//first rectangle in sequence
            int h = array[start];
            for(int end = start; end < array.length; end++){//last rectangle in sequence
                h = Math.min(h, array[end]);
                max = Math.max((end - start + 1) * h, max);
            }
        }
        return max;
    }
    //use divide and conquer technique
    public static long maxHistogram(int[] array, int start, int end) {
        if(start == end){
            return array[start];
        }
        int mid = (start + end) / 2;
        long leftMax = maxHistogram(array, start, mid);
        long rightMax = maxHistogram(array, mid + 1, end);
        long h = Math.min(array[mid], array[mid + 1]);
        int i = mid, j = mid + 1;
        long middleMax = h * (j - i + 1);
        while(i - 1 >= start || j + 1 <= end){
            while(i - 1 >= start && array[i - 1] >= h){
                i--;
            }
            while(j + 1 <= end && array[j + 1] >= h){
                j++;
            }
            middleMax = Math.max(middleMax,(j - i + 1) * h);
            if(i - 1 >= start && j + 1 <= end) {
                h = Math.max(array[i - 1], array[j + 1]);
            } else if(i - 1 >= start){
                h = array[i - 1];
            } else if(j + 1 <= end){
                h = array[j + 1];
            }
        }
        return Math.max(leftMax, Math.max(rightMax, middleMax));

    }

    public static void main(String[] args) {
//        Random r = new Random();
////        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//        int[] array = new int[100000];
//        for(int i = 0; i < array.length; i++){
//            array[i] = 1000000000;
//        }
//        long start = System.nanoTime();
//        System.out.println(maxHistogram(array, 0, array.length - 1));
//        long end = System.nanoTime();
//        System.out.printf("%.5f ms\n", (end - start) / 1000000.);
        String[] numbers;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            while(true) {
                numbers = br.readLine().split(" ");
                if(numbers.length == 1){
                    break;
                } else {
                    int n = Integer.valueOf(numbers[0]);
                    int[] array = new int[n];
                    for(int i = 0; i < n; i++){
                        array[i] = Integer.valueOf(numbers[i + 1]);
                    }
                    System.out.println(maxHistogram(array, 0, array.length - 1));
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
