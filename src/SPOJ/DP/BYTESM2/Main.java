package SPOJ.DP.BYTESM2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created by vreal on 25/07/15.
 */
public class Main {
    public static int maxStones(int[][] chamber){
        int h = chamber.length;
        int w = chamber[0].length;
        int[][] memory = new int[h][w];
        for(int n = 0; n < w; n++){
            memory[0][n] = chamber[0][n];
        }
        int max;
        for(int x = 1; x < h; x++){
            for(int y = 0; y < w; y++){
                max = memory[x - 1][y];
                if(y > 0){
                    max = Math.max(max, memory[x - 1][y - 1]);
                }
                if(y < w - 1){
                    max = Math.max(max, memory[x - 1][y + 1]);
                }
                memory[x][y] = max + chamber[x][y];
            }
        }
        max = -1;
        for(int n = 0; n < w; n++){
            max = Math.max(max, memory[h-1][n]);
        }
        return max;
    }

    public static void stressTest(){
        int h = 100, w = 100;
        Random r = new Random();
        int[][] chamber = new int[h][w];
        for(int x = 0; x < h; x++){
            for(int y = 0; y < w; y++){
                chamber[x][y] = r.nextInt(100 + 1);
            }
        }
        System.out.println(maxStones(chamber));
    }

    public static void main(String[] args){
//        stressTest();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] numbers;
        int[][] chamber;
        try {
            int w, h;
            int t = Integer.valueOf(br.readLine().trim());
            for(int n = 0; n < t; n++){
                numbers = br.readLine().trim().split(" ");
                h = Integer.valueOf(numbers[0]);
                w = Integer.valueOf(numbers[1]);
                chamber = new int[h][w];
                for(int x = 0; x < h; x++){
                    numbers = br.readLine().trim().split(" ");
                    for(int y = 0; y < w; y++){
                        chamber[x][y] = Integer.valueOf(numbers[y]);
                    }
                }
                System.out.println(maxStones(chamber));
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
