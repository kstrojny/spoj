package SPOJ.DP.TRT;

import java.util.Scanner;

/**
 * Created by vreal on 03/08/15.
 */
public class Main {

    public static int maxValue(int[] treats, int[][] memory, int start, int end){
        int a = treats.length - (end - start);
        if(start == end){
            return a * treats[start];
        } else if(memory[start][end] > 0) {
            return memory[start][end];
        } else {
            int max = Math.max(maxValue(treats, memory, start + 1, end) + a * treats[start],
                    maxValue(treats, memory, start, end - 1) + a * treats[end]);
            memory[start][end] = max;
            return max;
        }
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] array = new int[n];
        int[][] memory = new int[n][n];
        for(int i = 0; i < n; i++){
            array[i] = sc.nextInt();
        }
        System.out.println(maxValue(array, memory, 0, n - 1));
//        Arrays.binarySearch();

//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        try{
//            int n = Integer.valueOf(br.readLine().trim());
//            int[] array = new int[n];
//            int[][] memory = new int[n][n];
//            for(int i = 0; i < n; i++){
//                array[i] = Integer.valueOf(br.readLine());
//            }
//            System.out.println(maxValue(array, memory, 0, n - 1));
//        }catch(IOException e){
//            e.printStackTrace();
//        }
//        int[] array = new int[]{1, 3, 1, 5, 2};
//        int[][] memory = new int[n][n];
//        System.out.println(maxValue(array, memory, 0, n - 1));
    }
}
