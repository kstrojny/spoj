package SPOJ.AGGRCOW;

import java.util.Arrays;

/**
 * Created by vreal on 09/07/15.
 */
public class Main {

    private static boolean checkMaxDistance(int[] array, int maxDistance, int cows){
        int maxCows = 1;
        int lastStallIndex = 0;
        for(int n = 1; n < array.length; n++){
            if(array[n] - array[lastStallIndex] >= maxDistance){
                maxCows++;
                lastStallIndex = n;
                if(maxCows >= cows){
                    return true;
                }
            }
        }
        return false;
    }

    //1, 2, 5, 10, 11
    private static int calcMaxCowDistance(int[] array, int c){
        Arrays.sort(array);  //stable sort with O(n *log(n))
        int maxDistance = (array[array.length - 1] - array[0]) / (c - 1); //maximum distance beetwen cows
        //we are looking for largest value for which answer is true
        //true, true, false, false, false
        int start = 0;
        int end = maxDistance;
        while(true){
            int middle = (start + end + 1) / 2;
            if(start == end){
                return start;
            }
            if(checkMaxDistance(array, middle, c)){
                start = middle;
            } else {
                end = middle - 1;
            }
        }
    }

    public static void main(String[] args){
//        Scanner sc = new Scanner(System.in);
//        int t = sc.nextInt();
////        List<Integer> nArray = new ArrayList<Integer>(t);
//        List<Integer> cArray = new ArrayList<Integer>(t);
//        List<int[]> stallArray = new ArrayList<int[]>(t);
//        for(int x = 0; x < t; x++) {
//            int n = sc.nextInt();
////            nArray.add(n);
//            cArray.add(sc.nextInt());
//            int[] array = new int[n];
//            for (int y = 0; y < n; y++) {
//                array[y] = sc.nextInt();
//            }
//            stallArray.add(array);
//
//        }
//        for(int x = 0; x < t; x++){
//            System.out.println(calcMaxCowDistance(stallArray.get(x), cArray.get(x)));
//        }
//        Random rand = new Random();
//        int[] array = new int[100000];
//        for (int n = 0; n < 100000; n++){
//            array[n] = rand.nextInt(1000000000);
//        }
        int[] array = {1, 4, 5, 7, 10};
        System.out.println(calcMaxCowDistance(array, 3));
//        System.out.println(calcMaxCowDistance(array, 10));
//        System.out.println(calcMaxCowDistance(array, 100));
//        System.out.println(calcMaxCowDistance(array, 1000));
//        System.out.println(calcMaxCowDistance(array, 10000));
//        System.out.println(calcMaxCowDistance(array, 100000));
    }
}
