package SPOJ.NHAY;

/**
 * Created by vreal on 30/07/15.
 */
public class Main {

    private static int[] getP(String pattern){
        int[] p = new int[pattern.length()];
        int a = 0;
        for(int b = 1; b < pattern.length(); b++){
            while(a > 0 && pattern.charAt(a) != pattern.charAt(b)){
                a = p[a - 1];
            }
            if(pattern.charAt(a) == pattern.charAt(b)){
                a++;
            }
            p[b] = a;
        }
        return p;
    }

    public static void KMP(String pattern, String text){
        int i = 1, j = 0;
        int[] p = getP(pattern);
        while(i < text.length() - pattern.length()){
            while(j < pattern.length() && text.charAt(i + j - 1) == pattern.charAt(j)){
                ++j;
            }
            if(j == pattern.length()){
                System.out.println(j);
            }
            if(j == 0){
                ++i;
            } else {
                i = i + Math.max(1, j - 1 - p[j - 1]);
            }
        }
    }

    public static void main(String[] args){
        KMP("na", "banananobano");
    }
}
