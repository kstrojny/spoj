package SPOJ.SMALL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by vreal on 21/08/15.
 */
public class Main {

    private static final int MOD = 1000000007;

    private static int[] eratoshenesSieve(int max){
        boolean[] array = new boolean[max + 1];
        int primesNumber = 0;
        Arrays.fill(array, true);
        for(int i = 2; i <= max; i++){
            if(array[i]){
                primesNumber++;
                for(int n = 2 * i; n <= max; n +=i){
                    array[n] = false;
                }
            }
        }
        int[] output = new int[primesNumber];
        int outputPosition = 0;
        for(int i = 2; i <= max; i++){
            if(array[i]){
                output[outputPosition] = i;
                outputPosition++;
            }
        }
        return output;
    }

    private static int small(int n, int[] primes){
        int prime, tmp;
        long result = 1L;
        for(int i = 0; i < primes.length && primes[i] <= n; i++){
            prime = tmp = primes[i];
            while(tmp <= n){
                tmp *= prime;
            }
            result = (result * (tmp / prime)) % MOD;
        }
        return (int)result;
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            int t = Integer.valueOf(br.readLine());
            int v, max = -1;
            int[] input = new int[t];
            for(int i = 0; i < t; i++){
                v = Integer.valueOf(br.readLine());
                max = Math.max(max, v);
                input[i] = v;
            }
            int[] primes = eratoshenesSieve(max);
            for(int i = 0; i < t; i++){
                System.out.println(small(input[i], primes));
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
