package SPOJ.GSS1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by vreal on 14/07/15.
 */
public class Main {

    static class Node{
        public int max;
        public int left;
        public int right;
        public int total;
        public int leftRange;
        public int rightRange;
    }

    private static int leftSonIndex(int i){
        return 2 * i + 1;
    }

    private static int rightSonIndex(int i){
        return 2 * i + 2;
    }

    private static Node mergeNodes(Node a, Node b){
        Node result = new Node();
        result.max = Math.max(a.max, Math.max(b.max, b.left + a.right));
        result.left = Math.max(a.left, a.total + b.left);
        result.right = Math.max(b.right, b.total + a.right);
        result.total = a.total + b.total;
        result.leftRange = Math.min(a.leftRange, b.leftRange);
        result.rightRange = Math.max(a.rightRange, b.rightRange);
        return result;
    }

    public static Node[] createSegmentedTree(int[] input){
        Node[] tree = new Node[2 * input.length - 1];
        int lSon, rSon;
        for(int i = input.length - 1; i >= input.length - 1; i++){
            tree[i + input.length - 1] = new Node();
            tree[i + input.length - 1].max = input[i];
            tree[i + input.length - 1].left = input[i];
            tree[i + input.length - 1].right = input[i];
            tree[i + input.length - 1].total = input[i];
            tree[i + input.length - 1].leftRange = i;
            tree[i + input.length - 1].rightRange = i;
        }
        for(int i = input.length - 2; i >= 0; i--){
            lSon = leftSonIndex(i);
            rSon = rightSonIndex(i);
            tree[i] = mergeNodes(tree[lSon], tree[rSon]);
        }
        return tree;
    }

    public static Node answerQuery(Node[] tree, int actualRoot, int x, int y){
        Node result;

        if(x <= tree[actualRoot].leftRange && tree[actualRoot].rightRange <= y){
            return tree[actualRoot];
        }
        // totally outside range
        if(x > tree[actualRoot].rightRange || y < tree[actualRoot].leftRange){
            result = new Node();
            result.max = Integer.MIN_VALUE;
            result.left = Integer.MIN_VALUE;
            result.right = Integer.MIN_VALUE;
            result.total = Integer.MIN_VALUE;
            result.leftRange =  tree[actualRoot].leftRange;
            result.rightRange =  tree[actualRoot].rightRange;
            return result;
        }
        Node leftNode = answerQuery(tree, leftSonIndex(actualRoot), x, y);
        Node rightNode = answerQuery(tree, rightSonIndex(actualRoot), x, y);
        result = mergeNodes(leftNode, rightNode);
        return result;
    }

    public static void main(String[] args){
//        int n[] = {-3, 2, 2, -1, 1, 3, -2, 4};
////        int n[] = {-1, 2, 3};
//        Node[] tree = createSegmentedTree(n);
//        System.out.println(answerQuery(tree, 0, 0, 7).max);//9
//        System.out.println(answerQuery(tree, 0, 7, 7).max);//4
//        System.out.println(answerQuery(tree, 0, 0, 1).max);//2
//        System.out.println(answerQuery(tree, 0, 4, 5).max);//4
//        System.out.println(answerQuery(tree, 0, 0, 3).max);//4
//        System.out.println(answerQuery(tree, 0, 0, 4).max);//4
//        System.out.println(answerQuery(tree, 0, 5, 7).max);//5
//        System.out.println(answerQuery(tree, 0, 0, 2).max);//4
////        System.out.println(answerQuery(tree, 0, 0, 1).max);//2
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String[] numbers;
            int n = Integer.valueOf(br.readLine().trim());
            numbers = br.readLine().trim().split(" ");
            int[] array = new int[n];
            for (n = 0; n < array.length; n++) {
                array[n] = Integer.valueOf(numbers[n]);
            }
            int m = Integer.valueOf(br.readLine().trim());
            int[] x = new int[m];
            int[] y = new int[m];
            for (n = 0; n < m; n++) {
                numbers = br.readLine().trim().split(" ");
                x[n] = Integer.valueOf(numbers[0]) - 1;  // -1 because they index arrays starting from 1
                y[n] = Integer.valueOf(numbers[1]) - 1;
            }
            Node[] tree = createSegmentedTree(array);
            for (n = 0; n < m; n++) {
                System.out.println(answerQuery(tree, 0, x[n], y[n]).max);
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
