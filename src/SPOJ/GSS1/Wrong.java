package SPOJ.GSS1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by vreal on 09/07/15.
 */
class EndMax {
    public int end;
    public int max;

    public EndMax(int end, int max){
        this.end = end;
        this.max = max;
    }
}

public class Wrong {

    private static ArrayList<EndMax> getEndMaxList(int[] array, int start, int end){
        int maxSoFar = array[start];
        int maxEndingHere = array[start];
        ArrayList<EndMax> endMaxList = new ArrayList<EndMax>();
        int[] maxEnding = new int[array.length];
        endMaxList.add(new EndMax(-1, 0));
        for(int n = start; n <= end; n++){
            maxEndingHere = Math.max(maxEndingHere + array[n], array[n]);
            maxSoFar = Math.max(maxSoFar, maxEndingHere);
            maxEnding[n] = maxEndingHere;
            if(maxEndingHere <= 0 || n == end){
                EndMax endmax = new EndMax(n, maxSoFar);
                endMaxList.add(endmax);
                maxSoFar = -1000000000;
            }
        }
        return endMaxList;
    }

    private static int kadane(int[] array, int start, int end){
        int maxEndHere = array[start];
        int maxSoFar = array[start];
        for(int n = start + 1; n <= end; n++){
            maxEndHere = Math.max(array[n], array[n] + maxEndHere);
            maxSoFar = Math.max(maxSoFar, maxEndHere);
        }
        return maxSoFar;
    }
        //1, 2 4, 8, 10: y=4
    private static int searchEndIndex(ArrayList<EndMax> endMax, int y){
        int start = 0;
        int end = endMax.size() - 1;
        while(true){
            if(start == end){
                return start;
            }
            int middle = (start + end) / 2;
            if(endMax.get(middle).end < y){
                start = middle + 1;
            } else {
                end = middle;
            }
        }
    }

    private static int calcQuery(int[] array, ArrayList<EndMax> endMax, int x, int y){
        int maxSoFar = -1000000000;

        for(int n = searchEndIndex(endMax, y); endMax.get(n).end >= x; n--){  // change to binary search
            //1
            if(x > endMax.get(n - 1).end + 1 && y < endMax.get(n).end){
                maxSoFar = Math.max(maxSoFar, kadane(array, x, y));
            }
            //2
            else if(x > endMax.get(n - 1).end + 1 && y >= endMax.get(n).end){
                maxSoFar = Math.max(maxSoFar, kadane(array, x, endMax.get(n).end));
            }
            //3
            else if(x <= endMax.get(n - 1).end + 1 && y < endMax.get(n).end){
                maxSoFar = Math.max(maxSoFar, kadane(array, endMax.get(n - 1).end + 1, y));
            }
            //4
            else if(x <= endMax.get(n - 1).end + 1 && y >= endMax.get(n).end){
                maxSoFar = Math.max(maxSoFar, endMax.get(n).max);
            }
        }
        return maxSoFar;
    }

    public static void main(String[] args){
        int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        int[] array = new int[n];
        for(n = 0; n < array.length; n++){
            array[n] = sc.nextInt();
        }
        int m = sc.nextInt();
        int[] x = new int[m];
        int[] y = new int[m];
        for(n = 0; n < m; n++){
            x[n] = sc.nextInt() - 1;  // -1 because they index arrays starting from 1
            y[n] = sc.nextInt() - 1;
        }
//        int[] array = {1, 2, -4, 4, -1, -2, -1, 3, 2, -3, 1};
//        int[] array = {-1, 2, 3};
        ArrayList<EndMax> endMax = getEndMaxList(array, 0, array.length - 1);
//
//        System.out.println(calcQuery(array, endMax, 0, 1));
//        System.out.println(calcQuery(array, endMax, 1, 7));
//        System.out.println(calcQuery(array, endMax, 0, 8));
//        System.out.println(calcQuery(array, endMax, 5, 9));
        for(n = 0; n < m; n++){
            System.out.println(calcQuery(array, endMax, x[n], y[n]));
        }
//        System.out.println(calcQuery(0, 4, array, maxQueryStartingAtIndex, indexOfLastElementInMaxSequence));
    }
}
