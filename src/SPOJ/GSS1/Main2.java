package SPOJ.GSS1;

import java.io.*;
import java.util.Random;

/**
 * Created by vreal on 15/08/15.
 */
public class Main2 {

    static class Node{
        int value;
        int leftVal;
        int rightVal;
        int sum;
    }

    private static int leftSon(int index){
        return 2 * index + 1;
    }

    private static int rightSon(int index){
        return 2 * index + 2;
    }

    private static Node[] createTree(int[] input){
        int n = input.length;
        int h = (int)Math.ceil(Math.log(n) / Math.log(2)) + 1;
        int size = (int)Math.pow(2, h) - 1;
        Node[] tree = new Node[size];
        tree[0] = updateNode(tree, input, 0, 0, n - 1);
        return tree;
    }

    private static Node updateNode(Node[] tree, int[] input, int currentNode, int leftRange, int rightRange){
        if(leftRange < rightRange) {
            int leftSon = leftSon(currentNode);
            int rightSon = rightSon(currentNode);
            int mid = (leftRange + rightRange) / 2;
            tree[leftSon] = updateNode(tree, input, leftSon, leftRange, mid);
            tree[rightSon] = updateNode(tree, input, rightSon, mid + 1, rightRange);
            Node father = merge(tree[leftSon], tree[rightSon]);
            return father;
        } else if(leftRange == rightRange){
            Node node = new Node();
            node.sum = node.leftVal = node.rightVal = node.value = input[leftRange];
            return node;
        } else {
            throw new IllegalArgumentException();
        }
    }

    private static Node merge(Node left, Node right){
        Node father = new Node();
        father.sum = left.sum + right.sum;
        father.leftVal = Math.max(left.leftVal, left.sum + right.leftVal);
        father.rightVal = Math.max(right.rightVal, right.sum + left.rightVal);
        father.value = Math.max(left.rightVal + right.leftVal, Math.max(left.value, right.value));
        return father;
    }

    public static int answerQuery(Node[] tree, int x, int y, int n){
        Node node = answerQuery(tree, x, y, 0, n - 1, 0);
        return node.value;

    }

//    @Nullable
    private static Node answerQuery(Node[] tree, int x, int y, int leftRange, int rightRange, int currentNode){
        if(rightRange < x || y < leftRange) { //totally outside range
            return null;
        }
        if(x <= leftRange && rightRange <= y){  //totally inside range
            return tree[currentNode];
        } else {   //partially inside range
            int mid = (leftRange + rightRange) / 2;
            int leftSon = leftSon(currentNode);
            int rightSon = rightSon(currentNode);
            Node left = answerQuery(tree, x, y, leftRange, mid, leftSon);
            Node right = answerQuery(tree, x, y, mid + 1, rightRange, rightSon);
            if(left != null && right != null){
                Node node = merge(left, right);
                return node;
            }
            return (left != null) ? left : right;
        }
    }

    public static void stressTest(){
        long start = System.nanoTime();
        int n = 50000, m = (int)1e5;
        Random r = new Random();
        int[] input = new int[n];
        for(int i = 0; i < n; i++){
            input[i] = r.nextInt(2 * 15007) - 15007;
        }
        Node[] tree = createTree(input);
        for(int i = 0; i < m; i++){
            int y = r.nextInt(n) +1;
            int x = r.nextInt(y);
            answerQuery(tree, x, y, n);
        }
        long end = System.nanoTime();
        System.out.println((end - start) / 1e6 + " milliseconds");
    }

    public static void main(String[] args){
//        stressTest();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in), 7 * 50000); //7 char per number, 50000 numbers in total
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        String[] numbers;
        try{
            int n = Integer.valueOf(br.readLine().trim());
            numbers = br.readLine().split(" ");
            int[] input = new int[n];
            for(int i = 0; i < n; ++i){
                input[i] = Integer.valueOf(numbers[i]);
            }
            Node[] tree = createTree(input);
            int x, y;
            String answer;
            int m = Integer.valueOf(br.readLine().trim());
            for(int i = 0; i < m; ++i){
                numbers = br.readLine().split(" ");
                x = Integer.valueOf(numbers[0]) - 1;
                y = Integer.valueOf(numbers[1]) - 1;
                answer = Integer.toString(answerQuery(tree, x, y, n));
                bw.write(answer);
                bw.newLine();
            }
            bw.flush();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
