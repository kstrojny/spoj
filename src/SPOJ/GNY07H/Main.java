package SPOJ.GNY07H;

import java.util.Scanner;

/**
 * Created by vreal on 14/07/15.
 */
public class Main {

    static class Position{
        public int x;
        public int y;
    }

    private static Position getNextPosition(Position actual, boolean[][] array, int w){
        Position nextPos = new Position();
        for(int x = actual.x; x < w; x++){
            for(int y = 0; y < 4; y++){
                if(array[x][y] == false){
                    nextPos.x = x;
                    nextPos.y = y;
                    return nextPos;
                }
            }
        }
        return null;
    }

    private static int checkMemory(boolean[] row){
        if(row[0] == false && row[1] == false && row[2] == false && row[3] == false){
            return 0;
        }
        if(row[0] == true && row[1] == false && row[2] == false && row[3] == true){
            return 1;
        }
        if(row[0] == false && row[1] == true && row[2] == true && row[3] == false){
            return 2;
        }
        return -1;
    }

    private static int bruteForce(int  w, boolean[][] array, Position pos, int[][] memory){
        int memoryCell = checkMemory(array[pos.x]);
        if(memoryCell != -1 && memory[pos.x][memoryCell] != -1){
            return memory[pos.x][memoryCell];
        }
        int numberOfPosibilities = 0;
        if(array[pos.x][pos.y] == false){
            if(pos.y + 1 < 4 && array[pos.x][pos.y + 1] == false){
                array[pos.x][pos.y] = true;
                array[pos.x][pos.y + 1] = true;
                Position nextPos = getNextPosition(pos, array, w);
                if(nextPos != null){
                    numberOfPosibilities += bruteForce(w, array, nextPos, memory);
                } else {
                    numberOfPosibilities += 1;
                }
                array[pos.x][pos.y] = false;
                array[pos.x][pos.y + 1] = false;
//                System.out.println(numberOfPosibilities);
            }
            if(pos.x + 1 < w && array[pos.x +1][pos.y] == false) {
                array[pos.x][pos.y] = true;
                array[pos.x + 1][pos.y] = true;
                Position nextPos = getNextPosition(pos, array, w);
                if (nextPos != null) {
                    numberOfPosibilities += bruteForce(w, array, nextPos, memory);
                } else {
                    numberOfPosibilities += 1;   //just filled all array
                }
                array[pos.x][pos.y] = false;
                array[pos.x + 1][pos.y] = false;
//                System.out.println(numberOfPosibilities);
            }
        }
        if(memoryCell != -1) {
            memory[pos.x][memoryCell] = numberOfPosibilities;
        }
        return numberOfPosibilities;
    }

    public static int runBruteForce(int w){
        boolean[][] array = new boolean[w][4];
        for(int x = 0; x < w; x++){
            for(int y = 0; y < 4; y++){
                array[x][y] = false;
            }
        }
        int[][] memory = new int[w][3];
        for(int x = 0; x < w; x++){
            for(int y = 0; y < 3; y++){
                memory[x][y] = -1;
            }
        }
        Position startPos = new Position();
        startPos.x = 0;
        startPos.y = 0;
        return bruteForce(w, array, startPos, memory);
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] input = new int[n];
        for(int i = 0; i < n; i++){
            input[i] = sc.nextInt();
        }
        for(int i = 0; i < n; i++){
            System.out.printf("%d %d\n", i + 1, runBruteForce(input[i]));
        }
//        long startTime = System.nanoTime();
//        System.out.printf("%d\n", runBruteForce(100));
//        long endTime = System.nanoTime();
//        System.out.println((endTime - startTime) / 1000000000.);
    }
}
