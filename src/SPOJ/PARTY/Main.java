package SPOJ.PARTY;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by vreal on 20/07/15.
 */
public class Main {

    //parites[1] = cost, fun

    public static int[] solve(int budget, int[] partiesCost, int[] partiesFun){
        int[][] memory = new int[budget][partiesCost.length];
        int bestBudget = 0, bestFun = 0;

        int cost = partiesCost[0];
        int fun = partiesFun[0];
        for(int n = cost; n < budget; n++){
            memory[n][0] = fun;
            if(memory[n][0] > bestFun || (memory[n][0] == bestFun && n < bestBudget)){
                bestFun = memory[n][0];
                bestBudget = n;
            }
        }
        for (int i = 1; i < partiesCost.length ; i++) {
            cost = partiesCost[i];
            fun = partiesFun[i];
            for(int n = 0; n < budget; n++) {
                if(cost <= n){
                    memory[n][i] = Math.max(memory[n - cost][i - 1] + fun, memory[n][i - 1]);
                } else {
                    memory[n][i] = memory[n][i - 1];
                }
                if(memory[n][i] > bestFun || (memory[n][i] == bestFun && n < bestBudget)){
                    bestFun = memory[n][i];
                    bestBudget = n;
                }
            }
        }
        int[] answer = new int [2];
        answer[0] = bestBudget;
        answer[1] = bestFun;
        return answer;
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int budget, n;
        String line;
        boolean end  = false;
        try {
            while(!end) {
                line = br.readLine();
                while(line.isEmpty()){
                    line = br.readLine();
                }
                String[] numbers = line.split(" ");
                budget = Integer.valueOf(numbers[0]);
                n = Integer.valueOf(numbers[1]);
                if (budget != 0 && n != 0) {
                    int[] partiesFun = new int[n];
                    int[] partiesCost = new int[n];
                    for (int i = 0; i < n; i++) {
                        numbers = br.readLine().split(" ");
                        partiesCost[i] = Integer.valueOf(numbers[0]);
                        partiesFun[i] = Integer.valueOf(numbers[1]);
                    }

                    int[] answer = solve(budget, partiesCost, partiesFun);
                    System.out.println(answer[0] + " " +  answer[1]);
                } else {
                    end = true;
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
