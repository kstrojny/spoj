package SPOJ.GRAPH.PT07Y;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by vreal on 22/07/15.
 */
public class Main {

    public static boolean isATree(List<Integer>[] edges){
        int actualVertex = 0;
        int visitedVertices = 0;
        //bfs implementation
        int[] statuses = new int[edges.length];     //0 - undiscovered, 1 - discovered, 2 - processed
        int[] parents = new int[edges.length];
        Queue<Integer> q = new LinkedList<Integer>();
        parents[actualVertex] = -1;
        q.add(actualVertex);
        while(!q.isEmpty()){
            actualVertex = q.remove();
            statuses[actualVertex] = 1;
            for(Integer neighbour : edges[actualVertex]){
                if(parents[actualVertex] != neighbour){
                    parents[neighbour] = actualVertex;
                }
                if(statuses[neighbour] == 0){
                    q.add(neighbour);
                } else if(parents[actualVertex] != neighbour){
                    return false;
                }
            }
            statuses[actualVertex] = 2;
            visitedVertices++;
        }
        if(visitedVertices == edges.length) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args){
        int a, b;
        String[] numbers;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            boolean answer;
            numbers = br.readLine().trim().split(" ");
            int n = Integer.valueOf(numbers[0]);
            int m = Integer.valueOf(numbers[1]);
            if(n - 1 != m){
                answer = false;
            } else {
                List<Integer>[] edges = new List[n];
                for (int i = 0; i < n; i++) {
                    edges[i] = new LinkedList<Integer>();
                }
                for (int i = 0; i < m; i++) {
                    numbers = br.readLine().trim().split(" ");
                    a = Integer.valueOf(numbers[0]) - 1;
                    b = Integer.valueOf(numbers[1]) - 1;
                    edges[a].add(b);
                    edges[b].add(a);
                }
                answer = isATree(edges);
            }
            if(answer){
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }

}
