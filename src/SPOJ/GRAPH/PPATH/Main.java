package SPOJ.GRAPH.PPATH;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by vreal on 24/08/15.
 */
public class Main {

    enum Status{
        DISCOVERED, PROCESSED;
    }

    public static boolean[] isPrimeArray(int arraySize){
        boolean[] isPrime = new boolean[arraySize];
        int loopEnd = (int)Math.sqrt(arraySize);
        Arrays.fill(isPrime, true);
        isPrime[0] = isPrime[1] = false;
        for(int i = 2; i <= loopEnd; i++){
            if(isPrime[i]) {
                for (int k = 2 * i; k < arraySize; k += i) {
                    isPrime[k] = false;
                }
            }
        }
        return isPrime;
    }

    private static List<Integer> getAllNeighbours(int number, boolean[] isPrime){
        List<Integer> answer = new LinkedList<>();
        int n1 = number / 1000;
        int n2 = (number / 100) % 10;
        int n3 = (number / 10) % 10;
        int n4 = number % 10;
        int t;
        for(int i = 0; i <= 9; i++){
            if(i != n1 && i > 0){
                t = i * 1000 + number % 1000;
                if(isPrime[t]) {
                    answer.add(t);
                }
            }
            if(n2 != i){
                t = n1 * 1000 + i * 100 + number % 100;
                if(isPrime[t]){
                    answer.add(t);
                }
            }
            if(n3 != i){
                t = n1 * 1000 + n2 * 100 + i * 10 + n4;
                if(isPrime[t]){
                    answer.add(t);
                }
            }
            if(n4 != i){
                t = number - n4 + i;
                if(isPrime[t]){
                    answer.add(t);
                }
            }
        }
        return answer;
    }

    public static int shortestPath(int start, int end, boolean[] isPrime){
        if(start == end){
            return 0;
        }
        HashMap<Integer, Integer> distance = new HashMap<>();
        HashMap<Integer, Status> statuses = new HashMap<>();
        distance.put(start, 0);
        Queue<Integer> q = new LinkedList<>();
        q.add(start);
        statuses.put(start, Status.DISCOVERED);
        while(!q.isEmpty()){
            Integer current = q.remove();
            int currentDistance = distance.get(current);
            for(Integer neighbour: getAllNeighbours(current, isPrime)){
                if(end == neighbour){
                    return currentDistance + 1;
                }
                if(!statuses.containsKey(neighbour)){
                    statuses.put(neighbour, Status.DISCOVERED);
                    q.add(neighbour);
                    distance.put(neighbour, currentDistance + 1);
                }
            }
            statuses.put(start, Status.PROCESSED);
        }
        return -1;      //should never happen
    }

    private static void test(){
        long startTime = System.nanoTime();
        boolean[] isPrime = isPrimeArray(10000);
        for(int i = 0; i < 100; i++){
            int a = shortestPath(8179, 1033, isPrime);
        }
        long endTime = System.nanoTime();
        System.out.printf("Test took %f ms", (endTime - startTime) / 1e6);
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int start, end;
        String[] numbers;
        try{
            int t = Integer.valueOf(br.readLine());
            boolean[] isPrime = isPrimeArray((int)1e4);
            for(int i = 0; i < t; i++){
                numbers = br.readLine().split(" ");
                start = Integer.valueOf(numbers[0]);
                end = Integer.valueOf(numbers[1]);
                System.out.println(shortestPath(start, end, isPrime));
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
