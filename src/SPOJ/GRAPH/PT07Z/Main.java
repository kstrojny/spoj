package SPOJ.GRAPH.PT07Z;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vreal on 22/07/15.
 */
public class Main {

    public static int runLongestPath(List<Integer>[] edges){
        int root = 0;
        int[] parents  = new int[edges.length];
        parents[root] = -1;
        int[] answer = maxTreeLength(edges, root, parents);
        return answer[1];
    }

    private static int[] maxTreeLength(List<Integer>[] edges, int actualNode, int[] parents) {
        int maxTreeLength = 0;
        int secondMaxTreeLength = 0;
        int maxSoFar = 0;
        if(edges[actualNode].size() == 1 && edges[actualNode].get(0) == parents[actualNode]){
            return new int[]{0, 0};
        }
        for(Integer son : edges[actualNode]){
            if(son != parents[actualNode]){
                parents[son] = actualNode;
                int[] answer = maxTreeLength(edges, son, parents);
                int sonsHigh = answer[0] + 1;
                maxSoFar = Math.max(maxSoFar, answer[1]);
                if(maxTreeLength < sonsHigh){
                    secondMaxTreeLength = maxTreeLength;
                    maxTreeLength = sonsHigh;
                } else if(secondMaxTreeLength < sonsHigh){
                    secondMaxTreeLength = sonsHigh;
                }

            }
        }
        maxSoFar = Math.max(maxSoFar, maxTreeLength + secondMaxTreeLength);
        return new int[] {maxTreeLength, maxSoFar};
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            int a, b, n = Integer.valueOf(br.readLine().trim());
            String[] numbers;
            List<Integer>[] edges = new List[n];
            for (int i = 0; i < n; i++) {
                edges[i] = new LinkedList<Integer>();
            }
            for(int i = 0; i < n - 1; i++){
                numbers = br.readLine().trim().split(" ");
                a = Integer.valueOf(numbers[0]) - 1;
                b = Integer.valueOf(numbers[1]) - 1;
                edges[a].add(b);
                edges[b].add(a);
            }
            System.out.println(runLongestPath(edges));
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
