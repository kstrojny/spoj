package SPOJ.GRAPH.CHICAGO;

/**
 * Created by vreal on 08/08/15.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;


class Vertex{
    LinkedList<Edge> edges;
    double minDistance;
    int queueIndex;
    public Vertex(){
        this.edges = new LinkedList<>();
    }
}

class Edge{
    public double cost;
    public Vertex connectedTo;

    public Edge(double cost, Vertex connectedTo){
        this.cost = cost;
        this.connectedTo = connectedTo;
    }
}

class MyMinQueue {

    private ArrayList<Vertex> array;
    private int size = 0;

    public MyMinQueue(int initSize){
        array = new ArrayList<>(initSize);
    }

    public MyMinQueue(Vertex[] initArray){
        this.array = new ArrayList<>(initArray.length);
        for(int i = 0; i < initArray.length - 1;i++){
            this.array.add(i, initArray[i]);
            initArray[i].queueIndex = i;
            bubbleDown(i);
        }
        size = initArray.length - 1;
    }

    private int leftSon(int index){
        return 2 * index + 1;
    }

    private int rightSon(int index){
        return 2 * index + 2;
    }

    private int parent(int index){
        return (index - 1) / 2;
    }

    public void bubbleDown(int index){
        int l = leftSon(index);
        int p = rightSon(index);
        if(p < size){
            if(array.get(l).minDistance > array.get(p).minDistance){
                if(array.get(l).minDistance > array.get(index).minDistance){
                    swap(l, index);
                    bubbleDown(l);
                }
            } else {
                if(array.get(p).minDistance > array.get(index).minDistance){
                    swap(p, index);
                    bubbleDown(p);
                }
            }
        } else if(l == size - 1){
            if(array.get(l).minDistance > array.get(index).minDistance){
                swap(l, index);
            }
        }
    }

    public void bubbleUp(int index){
        if(index == 0){
            return;
        }
        if(array.get(parent(index)).minDistance > array.get(index).minDistance){
            swap(parent(index), index);
            bubbleUp(parent(index));
        }
    }

    private void swap(int i, int j){
        Vertex tmp = array.get(i);
        array.set(i, tmp);
        array.get(i).queueIndex = i;
        array.set(j, tmp);
        array.get(j).queueIndex = j;
    }

    public Vertex peek(){
        return array.get(0);
    }

    public Vertex remove(){
        Vertex v = array.get(0);
        swap(0, size-1);
        array.set(size - 1, null);
        size--;
        bubbleDown(0);
        return v;
    }

    public void add(Vertex v){
        array.add(v);
        size++;
        bubbleUp(size - 1);
    }

    public void updateVertex(int index, double newMinDistance){
        array.get(index).minDistance = newMinDistance;
        bubbleUp(index);
    }

    public int size(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

}


public class Main {

    //returns shortest path between two vertices
    public static double dijkstra(Vertex[] vertices, Vertex start, Vertex end){
        for(int i = 0; i < vertices.length; i++){
            vertices[i].minDistance = Double.NEGATIVE_INFINITY;
        }
        start.minDistance = 0.;
        MyMinQueue q = new MyMinQueue(vertices);

        while(!q.isEmpty()){
            Vertex current = q.remove();
            if(current.equals(end)){
                return end.minDistance;
            }
            for(Edge neighbour : current.edges){
                if(neighbour.connectedTo.minDistance < current.minDistance + neighbour.cost){
                    q.updateVertex(neighbour.connectedTo.queueIndex,  current.minDistance + neighbour.cost);
                }
            }
        }
        return 666.;
    }

    public static void main(String[] args){
        int a, b;
        double p;
        String[] numbers;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            while(true) {
                numbers = br.readLine().split(" ");
                if(numbers.length == 1){
                    break;
                }
                int n = Integer.valueOf(numbers[0]);
                int m = Integer.valueOf(numbers[1]);
                Vertex[] vertices = new Vertex[n];
                for(int i = 0; i < n; i++){
                    vertices[i] = new Vertex();
                }
                for(int i = 0; i < m; i++){
                    numbers = br.readLine().split(" ");
                    a = Integer.valueOf(numbers[0]) - 1;
                    b = Integer.valueOf(numbers[1]) - 1;
                    p = Math.log(Integer.valueOf(numbers[2]) / 100.);
                    vertices[a].edges.add(new Edge(p, vertices[b]));
                    vertices[b].edges.add(new Edge(p, vertices[a]));
                }
                double answer = dijkstra(vertices, vertices[0], vertices[vertices.length - 1]);
                answer = Math.exp(answer) * 100;
                System.out.printf("%.6f percent\n", answer);
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
