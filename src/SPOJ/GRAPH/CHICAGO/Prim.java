package SPOJ.GRAPH.CHICAGO;

/**
 * Created by vreal on 09/08/15.
 */

import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Prim {

    class Graph{
        Vertex[] vertices;

    }

    class Edge{
        double cost;
        Vertex v1;
        Vertex v2;

        public Vertex getSecond(Vertex v){
            if(v1.equals(v)){
                return v2;
            } else {
                return v1;
            }
        }
    }

    class Vertex{
        LinkedList<Edge> edges;
    }

    public static Set<Vertex> prim(Graph g){
        Set<Vertex> minimumSpanningTree = new TreeSet<>();
        Vertex v = g.vertices[0];
        minimumSpanningTree.add(v);


        return minimumSpanningTree;
    }

    private static boolean isCrossingEdge(Edge edge, Map<Vertex, Boolean> inTree){
        return inTree.containsKey(edge.v1) ^ inTree.containsKey(edge.v2);
    }
}
