package SPOJ.GRAPH.CHICAGO;

/**
 * Created by vreal on 08/08/15.
 */

import java.util.Arrays;

public class Dijkstra {
    int V = 10;
    int[][] matrix;

    void dijkstra(){
        int current = 0;

        int[] c = new int[V];//cost of getting to node
        Arrays.fill(c, Integer.MAX_VALUE);
        c[current] = 0;

        int[] e = new int[V];//last node in best path to that node
        Arrays.fill(e, -1);
        e[current] = current;

        boolean[] processed = new boolean[V];
        Arrays.fill(processed, false);

        for(int i = 0; i < V; i++) {
            for (Integer neighbour : matrix[current]) {
                if (matrix[current][neighbour] > 0) {
                    if (c[neighbour] > c[current] + matrix[current][neighbour]) {
                        c[neighbour] = c[current] + matrix[current][neighbour];
                        e[neighbour] = current;
                    }
                }
            }
            processed[current] = true;
            current = bestVertex(c, e, processed);
        }
    }

    private int bestVertex(int[] c, int[] e, boolean[] processed){
        int bestCost = Integer.MAX_VALUE;
        int bestVertex = -1;
        for(int i = 0; i < V; i++){
            if(c[i] < bestCost && !processed[i]){
                bestCost = c[i];
                bestVertex = i;
            }
        }
        return bestVertex;
    }


}
