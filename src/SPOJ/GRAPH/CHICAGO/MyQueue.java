package SPOJ.GRAPH.CHICAGO;

import java.util.ArrayList;

/**
 * Created by vreal on 08/08/15.
 */

//Implemented using min binary Heap
public class MyQueue {

    private ArrayList<Vertex> array;
    private int size = 0;

    public MyQueue(int initSize){
        array = new ArrayList<Vertex>(initSize);
    }

    public MyQueue(Vertex[] array){
        for(int i = 0; i < array.length - 1;i++){
            this.array.add(i, array[i]);
        }
        size = array.length - 1;
    }

    private int leftSon(int index){
        return 2 * index + 1;
    }

    private int rightSon(int index){
        return 2 * index + 2;
    }

    private int parent(int index){
        return (index - 1) / 2;
    }

    public void bubbleDown(int index){
        int l = leftSon(index);
        int p = rightSon(index);
        if(p < size){
            if(array.get(l).minDistance > array.get(p).minDistance){
                if(array.get(l).minDistance > array.get(index).minDistance){
                    swap(l, index);
                    bubbleDown(l);
                }
            } else {
                if(array.get(p).minDistance > array.get(index).minDistance){
                    swap(p, index);
                    bubbleDown(p);
                }
            }
        } else if(l == size - 1){
            if(array.get(l).minDistance > array.get(index).minDistance){
                swap(l, index);
            }
        }
    }

    public void bubbleUp(int index){
        if(index == 0){
            return;
        }
        if(array.get(parent(index)).minDistance > array.get(index).minDistance){
            swap(parent(index), index);
            bubbleUp(parent(index));
        }
    }

    private void swap(int i, int j){
        Vertex tmp = array.get(i);
        array.set(i, tmp);
        array.set(j, tmp);
    }

    public Vertex peek(){
        return array.get(0);
    }

    public Vertex remove(){
        Vertex v = array.get(0);
        swap(0, size-1);
        array.set(size - 1, null);
        size--;
        bubbleDown(0);
        return v;
    }

    public void add(Vertex v){
        array.add(v);
        size++;
        bubbleUp(size - 1);
    }

}
