package SPOJ.GRAPH.CAPCITY;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by vreal on 17/07/15.
 */
public class Main {

    public static List<Integer> tour(List<Integer>[] sons, List<Integer>[] fathers, int v){

        int[] statuses = new int[sons.length];       //0 undiscovered, 1 discovered, 2 processed
        int processedVertices = 0;
        Queue<Integer> q = new LinkedList<Integer>();
        while (processedVertices < sons.length) {
            processedVertices += bfs(q, v, statuses, fathers);
            if (processedVertices == sons.length) {
                //v is our first super winner
                Arrays.fill(statuses, 0);
                List<Integer> tmp = bfsWithResultList(q, v, statuses, sons);

                return tmp;
            } else if (!sons[v].isEmpty()) {
                v = sons[v].get(0);
            } else {
                return new ArrayList<Integer>();
            }
        }
        //throw some Exception
        throw new InternalError();
    }


    //count how many new vertices loses with v
    private static int bfs(Queue<Integer> q, int v, int[] statuses, List<Integer>[] losers){
        int processedVertex = 0;
        q.add(v);
        while(!q.isEmpty()){
            v = q.remove();
            statuses[v] = 1;
            for(Integer neighbour : losers[v]){
                if(statuses[neighbour] == 0){
                    statuses[neighbour] = 1;
                    q.add(neighbour);
                }
            }
            processedVertex++;
            statuses[v] = 2;
        }
        return processedVertex;
    }

    private static List<Integer> bfsWithResultList(Queue<Integer> q, int v, int[] statuses, List<Integer>[] edges) {
        List <Integer> result = new ArrayList<Integer>();
        statuses[v] = 1;
        q.add(v);
        int actual;
        while(!q.isEmpty()){
            actual = q.remove();
            for(Integer neighbour : edges[actual]){
                if(statuses[neighbour] == 0){
                    statuses[neighbour] = 1;
                    q.add(neighbour);
                }
            }
            statuses[actual] = 2;
            result.add(actual);
        }
        return result;
    }

    public static void main(String[] args){
        try {
            String line;
            String[] numbers;
            BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
            int a, b;

            numbers = bf.readLine().split(" ");
            int n = Integer.valueOf(numbers[0]);
            int m = Integer.valueOf(numbers[1]);
            LinkedList<Integer>[] fathers = new LinkedList[n];   //fathers[k] those who go to k
            LinkedList<Integer>[] sons = new LinkedList[n];    //sons[k] those cities where we can travel from k
            for(int k = 0; k < n; k++){
                fathers[k] = new LinkedList<Integer>();
                sons[k] = new LinkedList<Integer>();
            }
            for(int k = 0; k < m; k++){
                numbers = bf.readLine().split(" ");
                a = Integer.valueOf(numbers[0]) - 1;
                b = Integer.valueOf(numbers[1]) - 1;

                fathers[b].add(a);
                sons[a].add(b);
            }
            List<Integer> result = tour(sons, fathers, 3);
            Collections.sort(result);
            System.out.println(result.size());
            for(int k = 0; k < result.size(); k++){
                System.out.print(result.get(k) + 1);
                if(k != result.size() - 1){
                    System.out.print(" ");
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
