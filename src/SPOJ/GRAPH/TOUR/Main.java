package SPOJ.GRAPH.TOUR;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by vreal on 17/07/15.
 */
public class Main {

    public static int tour(List<Integer>[] winners, List<Integer>[] losers, int v){

        int[] statuses = new int[winners.length];       //0 undiscovered, 1 discovered, 2 processed
        int processedVertices = 0;
        Queue<Integer> q = new LinkedList<Integer>();
        while (processedVertices < winners.length) {
            processedVertices += bfs(q, v, statuses, losers);
            if (processedVertices == winners.length) {
                //v is our first super winner
                Arrays.fill(statuses, 0);
                int tmp = bfs(q, v, statuses, winners);
//                System.out.println(tmp);
                return tmp;
            }
            if (!winners[v].isEmpty()) {
                v = winners[v].get(0);
            } else {
                return 0;
            }
        }
        //throw some Exception
        return -1;
    }


    //count how many new vertices loses with v
    private static int bfs(Queue<Integer> q, int v, int[] statuses, List<Integer>[] losers){
        int processedVertex = 0;
        q.add(v);
        while(!q.isEmpty()){
            v = q.remove();
            statuses[v] = 1;
            for(Integer neighbour : losers[v]){
                if(statuses[neighbour] == 0){
                    statuses[neighbour] = 1;
                    q.add(neighbour);
                }
            }
            processedVertex++;
            statuses[v] = 2;
        }
        return processedVertex;
    }

    public static void main(String[] args){
        try {
            String line;
            String[] numbers;
            BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
            line = bf.readLine();
            int val, t = Integer.valueOf(line);
            List<Integer> output = new ArrayList<Integer>(t);
            for(int i = 0; i < t; i++){
                int n = Integer.valueOf(bf.readLine());
                LinkedList<Integer>[] winners = new LinkedList[n];   //winners[k] those who win with k
                LinkedList<Integer>[] losers = new LinkedList[n];    //losers[k] those who lose with k
                for(int k = 0; k < n; k++){
                    winners[k] = new LinkedList<Integer>();
                    losers[k] = new LinkedList<Integer>();
                }
                for(int k = 0; k < n; k++){
                    numbers = bf.readLine().split(" ");
                    for(int j = 1; j < numbers.length; j++){
                        val = Integer.valueOf(numbers[j]) - 1;
                        winners[k].add(val);
                        losers[val].add(k);
                    }
                }
                for(int k = 0; k < n; k++)
                    output.add(tour(winners, losers, k));
            }
            for(Integer result : output){
                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
/*
*

1
7
1 4
1 1
3 2 1 7
1 3
3 4 6 7
1 3
1 1


5
0
1 1
1 2
1 1
1 4


* */