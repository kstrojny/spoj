package SPOJ.MISERMAN;

import java.util.Scanner;

/**
 * Created by vreal on 14/07/15.
 */
public class Main {

    public static int getBestPath(int[][] grid, int row, int bus, int maxBus, int[][] bestPaths){
        if(row == 0){
            return grid[0][bus];
        }
        if(bestPaths[row][bus] != 0){
            return bestPaths[row][bus];
        }
        int bestPath = getBestPath(grid, row - 1, bus, maxBus, bestPaths);
        if(bus - 1 >= 0){
            bestPath = Math.min(bestPath, getBestPath(grid, row - 1, bus - 1, maxBus, bestPaths));
        }
        if(bus + 1 < maxBus){
            bestPath = Math.min(bestPath, getBestPath(grid, row - 1, bus + 1, maxBus, bestPaths));
        }
        int tmp = bestPath + grid[row][bus];
        bestPaths[row][bus] = tmp;
        return tmp;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(), m = sc.nextInt();
        int[][] grid = new int[n][m];
        for(int x = 0; x < n; x ++){
            for(int y = 0; y < m; y++){
                grid[x][y] = sc.nextInt();
            }
        }
        int[][] bestPaths = new int[n][m];
        int bestPath = Integer.MAX_VALUE;
        for(int i = 0; i < m; i++){
            bestPath = Math.min(bestPath, getBestPath(grid, n - 1, i, m, bestPaths));
        }
        System.out.println(bestPath);
    }

}
