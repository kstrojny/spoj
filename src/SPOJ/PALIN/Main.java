package SPOJ.PALIN;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by vreal on 09/06/15.
 * A positive integer is called a palindrome if its representation in the decimal system is the same when read from left
 * to right and from right to left. For a given positive integer K of not more than 1000000 digits, write the value of
 * the smallest palindrome larger than K to output. Numbers are always displayed without leading zeros.
 */

public class Main {
    /*
    * return positive number if first number is bigger than reversed second, negative otherwise. zero if they are equal
    * */
    private static int compareNumbers(char[] number, int start_n1, int start_n2){
        while(number[start_n1] - number[start_n2] == 0 && start_n1 > 0 && start_n2 < number.length){
            start_n1--;
            start_n2++;
        }
        if(start_n1 == -1 || start_n2 == number.length){
            return 0;
        }
        return number[start_n1] - number[start_n2];
    }

    private static void rewriteFirstHalf(char[] number){
        int l = number.length;
        for(int i = l/2; i < l; i++){
            number[i] = number[l - i - 1];
        }
    }
    //return true for '9999' case, false otherwise
    private static boolean increment(char[] number, int startPosition){
        while(startPosition >= 0) {
            if (number[startPosition] != '9') {
                number[startPosition] += 1;
                return false;
            } else {
                number[startPosition] = '0';
                startPosition -= 1;
//            return increment(number, startPosition - 1);
            }
        }
        return true; //case when we tried to increment number like '99999'
    }

    public static String getNextPalindrome(String line){
        if(line.equals("9"))
            return "11";
        if(line.length() == 1){
           return String.valueOf((char)(line.charAt(0) + 1));
        }
        char[] array = line.toCharArray();
        boolean allNineCase = false;
        if(array.length % 2 == 0){
            if(compareNumbers(array, array.length / 2 - 1, array.length / 2) <= 0){
                //we have to increase first half of array
                allNineCase = increment(array, array.length / 2 - 1);
            }
            rewriteFirstHalf(array);
        } else {
            if(compareNumbers(array, array.length / 2 - 1, array.length / 2 + 1) <= 0){
                //We have to increment middle number
                allNineCase = increment(array, array.length / 2);
            }
            rewriteFirstHalf(array);
        }
        if(allNineCase){
            if(array.length % 2 == 0) {
                array = new char[array.length + 1];
            } else {
                array = new char[array.length + 2];
            }
            for(int i = 1; i < array.length -1; i++){
                array[i] = '0';
            }
            array[0] = array[array.length - 1] = '1';
        }
        return String.valueOf(array);
    }

    private static void testGetNextPalindrome(){
        char[] line = new char[1000000];
        for(int i = 0; i < line.length; i++){
            line[i] = '9';
        }
        getNextPalindrome(String.valueOf(line));
    }

    public static void main(String[] args){
//        testGetNextPalindrome();
        Scanner scanner = new Scanner(System.in);
        int t = Integer.valueOf(scanner.nextLine());
        List<String> input = new ArrayList<String>();
        for(int i = 0; i < t; i++){
            String line = scanner.nextLine();
            input.add(line);
        }
        for(String line : input) {
            System.out.println(getNextPalindrome(line));
        }
    }
}
//1 2 3 2 1  #5
//0 1 2 3 4  #5